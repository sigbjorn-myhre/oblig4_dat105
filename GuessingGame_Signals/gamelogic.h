#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <QWidget>

#include <ctime>
#include <cstdlib>

class GameLogic : public QWidget {
    Q_OBJECT

    int gameSize;
    int guessValue; // Value to be guessed

    int generateRandom_c(); // Returns new random value

public:
    explicit GameLogic(int);
    virtual ~GameLogic();

signals:
    void newGameStarted(); // A new game has started
    void guessedIncorrect(int, int); // User guessed incorrectly
    void guessedCorrect(int); // User guessed correct

public slots:
    void newGame(); // Starts a new game
    void guess(int); // Checks guess from user
};

#endif // GAMELOGIC_H
