#include "gamelogic.h"

GameLogic::GameLogic(int gameSize) : gameSize(gameSize) {
    guessValue = generateRandom_c();
}

// TODO: destruction
GameLogic::~GameLogic() {}

// Slot for new game
void GameLogic::newGame() {
    guessValue = generateRandom_c();

    emit newGameStarted();
}

// Slot for new guess
void GameLogic::guess(int guessedValue) {
    if(guessedValue < guessValue) {
        emit guessedIncorrect(0, guessedValue);
    }
    else if (guessedValue > guessValue) {
        emit guessedIncorrect(guessedValue, gameSize-1);
    }
    else {
        emit guessedCorrect(guessedValue);
    }
}

// Generates a new random number
int GameLogic::generateRandom_c() {
    srand(time(NULL));
    return rand() % gameSize;
}
