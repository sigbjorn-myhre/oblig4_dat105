#include "gamepresentation.h"
#include "gamelogic.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    GamePresentation gamePresentation;
    GameLogic gameLogic(gamePresentation.getCount());

    // Connect buttons in presentation to gamelogic
    QObject::connect(gamePresentation.getSignalMapper(), SIGNAL(mapped(int)),
                     &gameLogic, SLOT(guess(int)));

    QObject::connect(gamePresentation.getNewGameBtn(), SIGNAL(clicked(bool)),
                     &gameLogic, SLOT(newGame()));

    // Connect emitted signals from logic to presentation
    QObject::connect(&gameLogic, SIGNAL(newGameStarted()),
                     &gamePresentation, SLOT(resetPresentation()));

    QObject::connect(&gameLogic, SIGNAL(guessedIncorrect(int,int)),
                     &gamePresentation, SLOT(incorrectButtons(int,int)));

    QObject::connect(&gameLogic, SIGNAL(guessedCorrect(int)),
                     &gamePresentation, SLOT(correctButton(int)));

    QObject::connect(&gameLogic, SIGNAL(guessedCorrect(int)),
                     &gamePresentation, SLOT(showMessageBox()));

    // Show window
    gamePresentation.show();

    return a.exec();
}
