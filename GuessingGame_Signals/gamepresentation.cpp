#include "gamepresentation.h"

GamePresentation::GamePresentation(QWidget *parent)
    : QWidget(parent) {
    // Parent layout
    hbox = new QHBoxLayout(this);

    // Signal mapper and buttons
    signalMapper = new QSignalMapper(this);
    for(int i = 0; i < count; ++i) {
        // Creating buttons
        buttons.push_back(new QPushButton(QString::number(i), this));
        hbox->addWidget(buttons.at(i));

        // And connecting mapper
        connect(buttons.at(i), SIGNAL(clicked(bool)), signalMapper, SLOT(map()));
        signalMapper->setMapping(buttons.at(i), i);
    }

    // Create newGameBtn
    newGameBtn = new QPushButton("&New game", this);
    hbox->addWidget(newGameBtn);

    // Create messagebox
    messageBox = new QMessageBox();
    messageBox->setText("Congratulations! You won!");
}

// TODO: Destruction
GamePresentation::~GamePresentation() {  }

// Resets presentation. Enables buttons and resets stylesheets
void GamePresentation::resetPresentation() {
    for(QPushButton *b : buttons) {
        b->setStyleSheet(""); // Resets stylesheet
        b->setEnabled(true);
    }
}

// Colors and disables a region of buttons
void GamePresentation::incorrectButtons(int from, int to) {
    for(int i = from; i <= to; i++) {
        buttons[i]->setStyleSheet("* { background-color: red } ");
        buttons[i]->setEnabled(false);
    }
}

// Colors correct button and disables the rest of the buttons
void GamePresentation::correctButton(int value) {
    buttons[value]->setStyleSheet("* { background-color: green } ");

    // Disable the rest of the buttons
    for(QPushButton *b : buttons) {
        b->setEnabled(false);
    }

}

// Shows a messagebox
void GamePresentation::showMessageBox() {
    messageBox->show();
}

// Getters
QPushButton* GamePresentation::getNewGameBtn() { return this->newGameBtn; }
QSignalMapper* GamePresentation::getSignalMapper() { return this->signalMapper; }
int GamePresentation::getCount() { return this->count; }
