#ifndef MAILCLIENTMODEL_H
#define MAILCLIENTMODEL_H

#include <QWidget>
#include <QAbstractListModel>
#include <vector>
#include "mail.h"

class MailClientModel : public QAbstractListModel {
    Q_OBJECT

    // Velger å holde data i denne klassen i en vector.
    // Kunne valgt å lagret i en tekstfil eller database.
    std::vector<Mail*> dataStore;

public:
    explicit MailClientModel();
    ~MailClientModel();

    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex&, Mail*, int role);
    int rowCount(const QModelIndex &parent) const;

public slots:
    void addValue(Mail*);
    void removeValue(QModelIndex);
};

#endif // MAILCLIENTMODEL_H
