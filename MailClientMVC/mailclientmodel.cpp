#include "mailclientmodel.h"

MailClientModel::MailClientModel() {
    this->addValue(new Mail("Ola Nordmann",
                            "Test",
                            "Hei, dette er en test."));
    dataStore.push_back(new Mail(
                            "Kari Nordmann",
                            "Test 2",
                            "Dette er også en test"));
}

MailClientModel::~MailClientModel() {}

QVariant MailClientModel::data(const QModelIndex &index, int role) const {
    if(role == Qt::DisplayRole) {
        return QString::fromStdString( dataStore.at(index.row())->toString());
    }
    else {
        return QVariant(); // tomt objekt
    }
}

int MailClientModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return dataStore.size();
}

void MailClientModel::addValue(Mail* mail) {
    beginInsertRows(QModelIndex(), 0, dataStore.size());
    dataStore.push_back(mail);
    endInsertRows();
}

bool MailClientModel::setData(const QModelIndex &index, Mail *item, int role) {
    bool dataSet = false;
    if(role == Qt::EditRole) {
        dataStore.push_back(item);
        emit dataChanged(index, index);
    }

    return dataSet;
}

void MailClientModel::removeValue(QModelIndex index) {
    int row = index.row();
    beginRemoveRows(QModelIndex(), row, row);
    dataStore.erase(dataStore.begin()+row);
    endRemoveRows();
}
