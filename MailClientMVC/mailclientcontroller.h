#ifndef MAILCLIENTCONTROLLER_H
#define MAILCLIENTCONTROLLER_H

#include <QObject>

#include "mailclientmodel.h"
#include "mailclientview.h"
#include "composemailview.h"

class MailClientController : public QObject {
    Q_OBJECT

    MailClientModel *model;
    MailClientView *client;
    ComposeMailView *compose;

public:
    MailClientController(MailClientModel*, MailClientView*, ComposeMailView*);

public slots:
    void checkMail();
    void deleteMail();
    void newMail();

    void commitMail();
    void discardMail();
};

#endif // MAILCLIENTCONTROLLER_H
