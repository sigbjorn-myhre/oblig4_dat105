#ifndef MAILCLIENTMODELINTERFACE_H
#define MAILCLIENTMODELINTERFACE_H

#include <QObject>
#include "mail.h"

class MailClientModelInterface {
public:
    virtual ~MailClientModelInterface(){}

signals:
    virtual void modelUpdated() = 0;
public slots:
    virtual void addValue(Mail*) = 0;
    virtual void addValue(QObject*) = 0;
    virtual void removeValue(QModelIndex) = 0;
};

Q_DECLARE_INTERFACE(MailClientModelInterface, "MailClientModelInterface")

#endif // MAILCLIENTMODELINTERFACE_H
