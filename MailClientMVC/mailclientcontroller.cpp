#include "mailclientcontroller.h"

MailClientController::MailClientController(MailClientModel *model,
                                           MailClientView* client,
                                           ComposeMailView* compose)
    : model(model), client(client), compose(compose) {  }

void MailClientController::checkMail() {
    qDebug("Check Mail");
}

// Deletes selected mail
void MailClientController::deleteMail() {
    model->removeValue(client->getCurrentIndex());
}

// Opens compose view
void MailClientController::newMail() {
    compose->show();
}

// Saves mail to model
void MailClientController::commitMail() {
    model->addValue(compose->getItem());

    compose->clearView();
    compose->close();
}

// Discards mail from compose view
void MailClientController::discardMail() {
    compose->clearView();
    compose->close();
}
