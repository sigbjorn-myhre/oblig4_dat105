#include "mailclientview.h"

MailClientView::MailClientView(QWidget *parent)
    : QWidget(parent) {
    // hbox and window
    hbox = new QHBoxLayout(this);
    setLayout(hbox);
    setGeometry(0, 0, 600, 350);
    setWindowTitle("My Little Mail Client");

    // Buttons
    checkBtn = new QPushButton("&Check", this);
    deleteBtn = new QPushButton("&Delete", this);
    newBtn = new QPushButton("&New...", this);

    // vbox for buttons
    vbox = new QVBoxLayout();
    vbox->addWidget(checkBtn);
    vbox->addWidget(deleteBtn);
    vbox->addWidget(newBtn);
    vbox->addStretch();

    // listview som viser epost
    listView = new QListView(this);

    // Add items to hbox
    hbox->addWidget(listView);
    hbox->addLayout(vbox);

    // Connect buttons to signals
    connect(checkBtn, SIGNAL(clicked(bool)), this, SIGNAL(checkMail()));
    connect(deleteBtn, SIGNAL(clicked(bool)), this, SIGNAL(deleteMail()));
    connect(newBtn, SIGNAL(clicked(bool)), this, SIGNAL(newMail()));
}

MailClientView::~MailClientView() {  }

QModelIndex MailClientView::getCurrentIndex() {
    return listView->currentIndex();
}

void MailClientView::setModel(QAbstractItemModel *model) {
    listView->setModel(model);
}
