#ifndef MAIL_H
#define MAIL_H

#include <QObject>
#include <string>

class Mail : public QObject {
    Q_OBJECT

public:
    Mail(std::string to, std::string subject, std::string text)
        : to(to), subject(subject), text(text){}

    std::string toString() {
        return to + " | " +
                subject + " | " +
                text;
    }

    std::string to;
    std::string subject;
    std::string text;
};

#endif // MAIL_H
