#include "composemailview.h"

ComposeMailView::ComposeMailView(QWidget *parent) : QDialog(parent) {
    // mainVBox and window
    mainVBox = new QVBoxLayout(this);
    setLayout(mainVBox);
    setGeometry(0, 0, 600, 450);
    setWindowTitle("Compose mail");

    // hboxes
    hbox1 = new QHBoxLayout();
    hbox2 = new QHBoxLayout();

    // vboxes
    vbox1 = new QVBoxLayout();
    vbox2 = new QVBoxLayout();

    // Labels
    toLabel = new QLabel("To:", this);
    vbox1->addWidget(toLabel);
    subjectLabel = new QLabel("Subject:", this);
    vbox1->addWidget(subjectLabel);
    hbox1->addLayout(vbox1);

    // LineEdits
    toLineEdit = new QLineEdit(this);
    vbox2->addWidget(toLineEdit);
    subjectLineEdit = new QLineEdit(this);
    vbox2->addWidget(subjectLineEdit);
    hbox1->addLayout(vbox2);

    // TextEdit
    textEdit = new QTextEdit(this);

    // Buttons
    sendBtn = new QPushButton("&Send", this);
    cancelBtn = new QPushButton("&Cancel", this);
    hbox2->addWidget(sendBtn);
    hbox2->addWidget(cancelBtn);
    hbox2->addStretch();

    // Add layouts
    mainVBox->addLayout(hbox1);
    mainVBox->addWidget(textEdit);
    mainVBox->addLayout(hbox2);

    // Connect buttons to other signals
    connect(sendBtn, SIGNAL(clicked(bool)), this, SIGNAL(accepted()));
    connect(cancelBtn, SIGNAL(clicked(bool)), this, SIGNAL(rejected()));
}

void ComposeMailView::clearView() {
    toLineEdit->clear();
    subjectLineEdit->clear();
    textEdit->clear();
}

Mail* ComposeMailView::getItem() {
    return new Mail(
                toLineEdit->text().toStdString(),
                subjectLineEdit->text().toStdString(),
                textEdit->toPlainText().toStdString());
}
