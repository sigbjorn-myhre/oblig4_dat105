#ifndef MAILCLIENTVIEW_H
#define MAILCLIENTVIEW_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QListView>

class MailClientView : public QWidget {
    Q_OBJECT

    QHBoxLayout *hbox;
    QVBoxLayout *vbox;

    QPushButton *checkBtn;
    QPushButton *deleteBtn;
    QPushButton *newBtn;

    QListView *listView;

public:
    MailClientView(QWidget *parent = 0);
    ~MailClientView();

    QModelIndex getCurrentIndex();
    void setModel(QAbstractItemModel*);

signals:
    void checkMail();
    void deleteMail();
    void newMail();
};

#endif // MAILCLIENTVIEW_H
