#ifndef COMPOSEMAILVIEW_H
#define COMPOSEMAILVIEW_H

#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSignalMapper>

#include "mailsignalmapper.h"
#include "mail.h"

class ComposeMailView : public QDialog {
    Q_OBJECT

    QVBoxLayout *mainVBox;
    QVBoxLayout *vbox1;
    QVBoxLayout *vbox2;
    QHBoxLayout *hbox1;
    QHBoxLayout *hbox2;

    QLabel *toLabel;
    QLabel *subjectLabel;

    QLineEdit *toLineEdit;
    QLineEdit *subjectLineEdit;

    QPushButton *sendBtn;
    QPushButton *cancelBtn;

    QTextEdit *textEdit;

public:
    explicit ComposeMailView(QWidget *parent = 0);

    Mail* getItem();
    void clearView();
};

#endif // COMPOSEMAILVIEW_H
