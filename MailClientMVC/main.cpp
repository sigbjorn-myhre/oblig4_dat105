#include "mailclientview.h"
#include "mailclientmodel.h"
#include "mailclientcontroller.h"
#include "composemailview.h"

#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    MailClientView mcView;
    ComposeMailView cmView;

    MailClientModel mcModel;
    MailClientController mcController(&mcModel, &mcView, &cmView);

    // Connect buttons in views to controller
    QObject::connect(&mcView, SIGNAL(checkMail()),
                     &mcController, SLOT(checkMail()));
    QObject::connect(&mcView, SIGNAL(deleteMail()),
                     &mcController, SLOT(deleteMail()));
    QObject::connect(&mcView, SIGNAL(newMail()),
                     &mcController, SLOT(newMail()));

    QObject::connect(&cmView, SIGNAL(accepted()),
                     &mcController, SLOT(commitMail()));
    QObject::connect(&cmView, SIGNAL(rejected()),
                     &mcController, SLOT(discardMail()));

    // List model
    mcView.setModel(&mcModel);
    mcView.show();

    return a.exec();
}
