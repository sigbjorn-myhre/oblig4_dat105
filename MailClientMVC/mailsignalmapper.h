#ifndef MAILSIGNALMAPPER_H
#define MAILSIGNALMAPPER_H

#include <QObject>
#include <QSignalMapper>
#include "mail.h"

class MailSignalMapper : public QSignalMapper {
public:
    MailSignalMapper();
    void setMapping(QObject*, Mail*);

signals:
    void mapped(Mail*);
};

#endif // MAILSIGNALMAPPER_H
