#include "gamepresentation.h"
#include "gamepresentationinterface.h"
#include "gamelogic.h"
#include "gamelogicinterface.h"

#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    // Creating implementations
    GamePresentation gamePresentation;
    GameLogic gameLogic(gamePresentation.getCount());

    // Creating interfaces
    GamePresentationInterface *gpi = &gamePresentation;
    GameLogicInterface *gli = &gameLogic;

    // Connecting signals to interfaces
    gamePresentation.connectSignals(gli);
    gameLogic.connectSignals(gpi);

    // Show window
    gamePresentation.show();

    return a.exec();
}
