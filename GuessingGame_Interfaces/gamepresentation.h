#ifndef GAMEPRESENTATION_H
#define GAMEPRESENTATION_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSignalMapper>
#include <QMessageBox>

#include <vector>

#include "gamelogicinterface.h"
#include "gamepresentationinterface.h"

class GamePresentation : public QWidget, public GamePresentationInterface {
    Q_OBJECT
    Q_INTERFACES(GamePresentationInterface)

    QHBoxLayout *hbox;
    QPushButton *newGameBtn;
    std::vector<QPushButton*> buttons;
    QSignalMapper *signalMapper;
    QMessageBox *messageBox;

    const int count = 10; // number of buttons

public:
    GamePresentation(QWidget *parent = 0);
    ~GamePresentation();

    int getCount();
    void connectSignals(GameLogicInterface*);

public slots:
    void resetPresentation(); // oppdaterer slik at presentation er klar for nytt spill
    void incorrectButtons(int, int); // gjør knapper som er feil røde
    void correctButton(int); // gjør rett knapp grønn
    void showMessageBox(); // Shows a messagebox after user guessed correctly
};

#endif // GAMEPRESENTATION_H
