#ifndef GAMELOGICINTERFACE_H
#define GAMELOGICINTERFACE_H

#include <QObject>

class GameLogicInterface {
public:
    virtual ~GameLogicInterface(){}

signals:
    virtual void newGameStarted() = 0;
    virtual void guessedIncorrect(int, int) = 0;
    virtual void guessedCorrect(int) = 0;

public slots:
    virtual void newGame() = 0;
    virtual void guess(int) = 0;
};

Q_DECLARE_INTERFACE(GameLogicInterface, "GameLogicInterface")

#endif // GAMELOGICINTERFACE_H
